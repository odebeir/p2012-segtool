import sys
sys.path.append('../dependencies')

import os
import matplotlib.pyplot as plt

data_path = '../data'

fnames = os.walk(data_path)


for path,subdir,filenames in fnames:
    for fn in filenames:
        filepath = os.path.join(path,fn)
        root,ext = os.path.splitext(filepath)
        if ext == '.tif':
            data = plt.imread(filepath)
            plt.figure()
            plt.imshow(data)
            plt.show()


