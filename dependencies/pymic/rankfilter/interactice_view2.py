from matplotlib.widgets import  RectangleSelector
from pylab import *

def onselect(eclick, erelease):
  'eclick and erelease are matplotlib events at press and release'
  print ' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata)
  print ' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata)
  print ' used button   : ', eclick.button

def toggle_selector(event):
    print ' Key pressed.'
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print ' RectangleSelector deactivated.'
        toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print ' RectangleSelector activated.'
        toggle_selector.RS.set_active(True)

def main_fig():
    x = arange(100)/(99.0)
    y = sin(x)
    fig = figure
    ax = subplot(111)
    ax.plot(x,y)
    ax.imshow(np.random.rand(100,100)*255, interpolation='nearest')

    rectprops = dict(facecolor='red', edgecolor = 'black',alpha=0.5, fill=True)
    lineprops = dict(color='black', linestyle=':', linewidth = 10, alpha=0.5)
    RS = RectangleSelector(ax, onselect, drawtype='box', rectprops=rectprops, lineprops=lineprops)
    #toggle_selector.RS = RectangleSelector(ax, onselect, drawtype='box', rectprops=rectprops, lineprops=lineprops)
    #connect('key_press_event', toggle_selector)
    show()

if __name__=='__main__':
    main_fig()