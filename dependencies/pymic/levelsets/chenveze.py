import numpy as npy
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from scipy.ndimage import distance_transform_cdt,gaussian_filter
from scipy.ndimage.filters import convolve,convolve1d
from maskcircle2 import maskcircle2
from heaviside import heaviside
from kappa import kappa
from checkstop import checkstop


def chenveze(I,mask,num_iter,mu=.2,method='chan'):
    """Active contour with Chen-Vese Method
        for image segementation

        Implemented by Yue Wu (yue.wu@tufts.edu)
        Tufts University
        Feb 2009
        http://sites.google.com/site/rexstribeofimageprocessing/

        all rights reserved
        Last update 02/26/2009
        --------------------------------------------------------------------------
        Usage of varibles:
        input:
           I           = any gray/double/RGB input image
           mask        = initial mask, either customerlized or built-in
           num_iter    = total number of iterations
           mu          = chenvezeweight of length term
           method      = submethods pick from ('chen','vector','multiphase')

        Types of built-in mask functions
           'small'     = create a small circular mask
           'medium'    = create a medium circular mask
           'large'     = create a large circular mask
           'whole'     = create a mask with holes around
           'whole+small' = create a two layer mask with one layer small
                           circular mask and the other layer with holes around
                           (only work for method 'multiphase')
        Types of methods
           'chan'      = general CV method
           'vector'    = CV method for vector image
           'multiphase'= CV method for multiphase (2 phases applied here)

        output:
           phiS0        = updated level set function
    """

    eps = 1.0e-10

    #check mask
    mask = maskcircle2(I,mask)

    #initialisation
    P = I.astype(float)
    pmin = npy.min(P)
    pmax = npy.max(P)
    prange = pmax-pmin
    P = (P-pmin)/prange
    force = eps

    #core function
    if method == 'chan':
        #SDF
        #get the distance map of the initial mask
#        phi0 = distance_transform_cdt(mask) - distance_transform_cdt(1-mask) + mask - .5
        phi0 = distance_transform_cdt(mask) - distance_transform_cdt(1-mask) - mask

        #initial force, set to eps to avoid division by zeros

        #main loop
        fig = plt.figure()
        for n in range(num_iter):
            inidx = phi0>=0
            outidx = phi0<0

            L = P
            c1 = npy.sum(L*(heaviside(phi0)))/(npy.sum(inidx)+eps)
            c2 = npy.sum(L*(1.0-heaviside(phi0)))/(npy.sum(outidx)+eps)
            force_image = - (L-c1)**2 + (L-c2)**2
            #sum on all the band
            #todo

            plt.clf()

            plt.subplot(2,2,1)
            plt.imshow(force_image)
            plt.title('F')
            plt.colorbar()
            plt.subplot(2,2,2)
            plt.imshow(phi0)
            plt.title('phi')
            plt.colorbar()
            print n,c1,c2
            # calculate the external force of the image
            force = mu*kappa(phi0)/npy.max(npy.abs(kappa(phi0)))+force_image
            plt.subplot(2,2,3)
            plt.imshow(phi0<0)
            plt.title('seg')
            plt.colorbar()
            #display graylevel distribution for in and out
            hin = npy.histogram(P[inidx].flatten(),npy.arange(-1,1,.01))
            hout = npy.histogram(P[outidx].flatten(),npy.arange(-1,1,.01))
            plt.subplot(2,2,4)
            plt.plot(hin[0],label='in')
            plt.plot(hout[0],label='out')
            plt.title('distribution')
            plt.legend()
            fig.savefig('../../test/temp/iter%04d.png'%n)

            #normalize the force
            force = 1.0 * force / npy.max(npy.abs(force))

            #get step size dt
            dt = 10.0

            #stopping criterion
            old = phi0
            phi0 = phi0 + dt*force
            new = phi0

            indicator = checkstop(old,new,dt)

#            if false:
#                seg = phi0<0
#                return seg

    seg = phi0<0
    mu_in = npy.sum(I*(heaviside(phi0)))/(npy.sum(inidx)+eps)
    mu_out = npy.sum(I*(1.0-heaviside(phi0)))/(npy.sum(outidx)+eps)
    return [seg,mu_in,mu_out]


def Cx(ima):
    """x' derivative of image"""
    c = convolve1d(ima,npy.array([-1,0,1]),axis=1,cval=1)
    return c/2.0

def Cy(ima):
    """y' derivative of image"""
    c = convolve1d(ima,npy.array([-1,0,1]),axis=0,cval=1)
    return c/2.0

def border(I):
    Ix = Cx(I)
    Iy = Cy(I)
    return npy.maximum(npy.abs(Ix),npy.abs(Iy))

def local_orientation(I,s=1.5):
#    I = gaussian_filter(I,s)
    Ix = Cx(I)
    Iy = Cy(I)
    angle = npy.arctan2(Iy,Ix)
    angle = gaussian_filter(angle,s)
    return angle

def demo_texture():
#    I = plt.imread('../../test/data/texture.png')[:,:,0]
    I = plt.imread('../../test/data/hlines_mix2.tif')
    #add some noise
    n = npy.random.random(I.shape)
    n_th = .1
    I[n>n_th] = 255-I[n>n_th]

    A = local_orientation(I,3)

    plt.figure()
    plt.subplot(1,2,1)
    plt.imshow(I)
    plt.subplot(1,2,2)
    plt.imshow(A)
    plt.colorbar()

    S,mu_in,mu_out = chenveze(A,mask='large',num_iter=20,mu=.2,method='chan')

    print mu_in,mu_out

    plt.figure()
    plt.imshow(I,cmap=cm.gray)
    plt.imshow(S*255,cmap=cm.jet,alpha=.8)
    plt.xlabel('$\mu_{in}=%f ^{\circ}$  $\mu_{out}=%f ^{\circ}$'%(180*mu_in/npy.pi,180*mu_out/npy.pi))
    plt.show()


def main():

#    I = plt.imread('../../test/data/testpng.png')[:,:,0]

#    I = plt.imread('../../test/data/image4.png')[:,:,0]
#    I = local_orientation(I,1)
#    I = plt.imread('../../test/data/exp0012.jpg')
#    I = plt.imread('../../test/data/dh_phase.png')
#    I = plt.imread('../../test/data/brain.jpg')[:,:,1]
#    I = plt.imread('../../test/data/test.png')*255.0
#    I = plt.imread('../../test/data/flowers.jpg')[:,:,0]
#    I = plt.imread('../../test/data/cameraman.tif')
#    I = plt.imread('../../test/data/phase_s.png')
    print I.shape

    plt.imshow(I)
    plt.colorbar()
    plt.show()

    S = chenveze(I,mask='whole',num_iter=100,mu=.2,method='chan')
    plt.show()

    plt.imshow(S)
    plt.colorbar()
    plt.show()
if __name__ == '__main__':
    demo_texture()

