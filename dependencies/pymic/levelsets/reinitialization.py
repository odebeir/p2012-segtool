import numpy as npy

def reinitialization(D,dt):
    """reinitialize the distance map for active contour
    """
    m,n = D.shape
    T = npy.hstack((npy.zeros((m,1)),D,npy.zeros((m,1))))
    m,n = T.shape
    T = npy.vstack((npy.zeros((1,n)),T,npy.zeros((1,n))))

    a = D - T[:-2,1:-1]
    b = T[2:,1:-1] - D
    c = D - T[2:,:-2]
    d = T[1:-1,2:]-D

    a_p = npy.maximum(a,0)
    a_m = npy.minimum(a,0)
    b_p = npy.maximum(b,0)
    b_m = npy.minimum(b,0)
    c_p = npy.maximum(c,0)
    c_m = npy.minimum(c,0)
    d_p = npy.maximum(d,0)
    d_m = npy.minimum(d,0)

    G = npy.zeros(D.shape)
    ind_plus = D>0
    ind_minus = D<0

    G[ind_plus] = npy.sqrt(npy.maximum(a_p[ind_plus]**2,b_m[ind_plus]**2)
    + npy.maximum(c_p[ind_plus]**2,d_m[ind_plus]**2))-1
    G[ind_minus] = npy.sqrt(npy.maximum(a_m[ind_minus]**2,b_p[ind_minus]**2)
    + npy.maximum(c_m[ind_minus]**2,d_p[ind_minus]**2))-1

    sign_D = D/npy.sqrt(D**2+1)
    D = D - dt * sign_D * G

    return D

def main():
    import matplotlib.pyplot as plt

    X,Y = npy.meshgrid(npy.linspace(-1,1,200),npy.linspace(-1,1,200))
    Z = X**2+Y**2

    D = reinitialization(Z,1)

    plt.imshow(D)
    plt.show()

if __name__ == '__main__':
    main()

