import numpy as npy

def checkstop(old,new,dt):
    """indicate whether we should performance further iterations or stop
    """

    if new.ndim==2: #grayscale image
        ind = npy.abs(new)<=.5
        M = npy.sum(ind)
        print 'M',M
        Q = npy.sum(npy.abs(new[ind]-old[ind]))/M
        if Q<dt*.18**2:
            indicator = 1
        else:
            indicator = 0

    else: #rgb image
        new0 = new[:,:,0]
        new1 = new[:,:,1]
        old0 = old[:,:,0]
        old1 = old[:,:,1]
        ind0 = npy.abs(old0)<1
        ind1 = npy.abs(old1)<1
        M0 = npy.sum(ind0)
        M1 = npy.sum(ind1)
        Q0 = npy.sum(npy.abs(new0[ind0]-old0[ind0]))/M0
        Q1 = npy.sum(npy.abs(new1[ind1]-old1[ind1]))/M1
        if Q0<dt*.18**2 & Q1<dt*.18**2:
            indicator = 1
        else:
            indicator = 0

    return indicator

def main():
    print 'no test'

if __name__ == '__main__':
    main()

