# -*- coding: utf-8 -*-
'''
This module implement algorithms for local orientation analysis
.. code-block:: python

   from scipy import misc
   from rankfilter import rankfilter

   im = misc.imread('../../test/data/cameraman.tif')
   f = rankFilter(im,filtName = 'mean',radius = 30,verbose = False)
   misc.imsave('../../test/data/cameraman_filt_mean.tif',f)
   print f
'''
__author__ = 'olivier'

import numpy as np
from scipy import misc
from numpy.fft import fft2,ifft2,fftshift,ifftshift
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from matplotlib.widgets import  RectangleSelector

def onselect(eclick, erelease):
        'eclick and erelease are matplotlib events at press and release'
        print ' startposition : (%f, %f)' % (eclick.xdata, eclick.ydata)
        print ' endposition   : (%f, %f)' % (erelease.xdata, erelease.ydata)
        print ' used button   : ', eclick.button
        (xa,xb,ya,yb)=(min(eclick.xdata,erelease.xdata),max(eclick.xdata,erelease.xdata),
                       min(eclick.ydata,erelease.ydata),max(eclick.ydata,erelease.ydata))
#        crop = im[int(ya):int(yb),int(xa):int(xb)]
        
if __name__=='__main__':
    im = misc.imread('../../test/data/hlines_mix.tif')
    im = im.astype(float)
    F1 = fft2(im)
    ampl = np.abs(F1)
    phi = np.angle(F1)

    main_fig = plt.figure()
    ax_main = plt.subplot(1,1,1)

    plt.imshow(fftshift(ampl))
    plt.colorbar()

    F2 = ampl*np.cos(phi) + 1j*ampl*np.sin(phi)

    fima = ifft2(F2).real
    plt.figure()
    plt.imshow(fima,cmap=cm.gray)



    rectprops = dict(facecolor='red', edgecolor = 'red',alpha=0.5, fill=True)
    lineprops = dict(color='black', linestyle=':', linewidth = 10, alpha=0.5)

    rs = RectangleSelector(ax_main, onselect, drawtype='box', rectprops=rectprops,
                                       lineprops=lineprops,useblit=False)


    plt.show()

    
