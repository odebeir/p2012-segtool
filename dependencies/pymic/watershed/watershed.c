/*
	This watershed code is adapted to the WeavePython version using numpy arrays
 */

#include <stdlib.h>

int do_water(int mode, unsigned char *pin,unsigned char *pmark,unsigned int * plabel,
		int sizex, int sizey);

void modified_gradient(unsigned char *image,unsigned char *mark,int SizeX,int SizeY);
void segmentation(unsigned char *fi2,char merge,char color,int M,int N,unsigned int *label,int *zones);


int do_water(int mode, unsigned char *pin,unsigned char *pmark, unsigned int * plabel,	int sizex, int sizey)
{
	int i, j, rows, cols,zones;

	/* Check proper input and output. */

	/* Get input arguments. */
	rows = sizey;
	cols = sizex;

	switch(mode)
	{
	case 0://None
		break;
	case 1://Watershed
		segmentation(pin,1,0,cols,rows,plabel,&zones);
		break;
	case 2://Marked Watershed
		modified_gradient(pin,pmark,cols,rows);
		segmentation(pin,1,0,cols,rows,plabel,&zones);
		break;
	case 3://Marked Gradient Watershed
		modified_gradient(pin,pmark,cols,rows);
		break;
	}

	return zones;
}


/***********************************************************************/

#define _INIT_ -1
#define _MASK_ -2
#define _WSHED_ 0

int *queue;
int M,N,PrtFirst,PrtLast;

/***********************************************************************/
void invert(unsigned char *image,unsigned char *image_res,int SizeX,int SizeY)
{
	int i,j;

	for (j=0;j<SizeY;j++)
		for (i=0;i<SizeX;i++)
			image_res[i+j*SizeX]=255-image[i+j*SizeX];

}

/*************************************************************************/
void substract(unsigned char *image,unsigned char *image2,int SizeX,int SizeY)
{
	int i,j,coord;

	for (j=0;j<SizeY;j++)
		for (i=0;i<SizeX;i++)
		{
			coord=i+j*SizeX;
			if (image[coord]>image2[coord]) image[coord]=image[coord]-image2[coord];
			else image[coord]=0;
		}
}
/*************************************************************************/

void copy(unsigned char *image,unsigned char *image2,int SizeX,int SizeY)
{
	int i,j;

	for (j=0;j<SizeY;j++)
		for (i=0;i<SizeX;i++)
			image2[i+j*SizeX]=image[i+j*SizeX];
}

/*************************************************************************/

void copy_int(int *label,int *label_prec,int SizeX,int SizeY)
{
	int i,j;

	for (j=0;j<SizeY;j++)
		for (i=0;i<SizeX;i++)
			label_prec[i+j*SizeX]=label[i+j*SizeX];
}
/*************************************************************************/

void modified_gradient(unsigned char *image,unsigned char *mark,int SizeX,int SizeY)
{
	unsigned char *result,*aux;
	int *vec_histo,*list,*phisto_cumul,*phisto;
	int i,j,k,l,coord;
	int seuil;
	char ok,pass;

	if((result=(unsigned char *)calloc(SizeX*SizeY,sizeof(unsigned char)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if((aux=(unsigned char *)malloc(SizeX*SizeY*sizeof(unsigned char)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if((vec_histo=(int *)malloc(SizeX*SizeY*sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if((list=(int *)calloc(SizeX*SizeY,sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if ((phisto_cumul = (int *)malloc(256*sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if ((phisto = (int *)malloc(256*sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}

	substract(image,mark,SizeX,SizeY);
	invert(image,image,SizeX,SizeY);
	for (i=0;i<=255;i++) phisto[i]=0;
	for (j=1;j<(SizeY-1);j++)
		for (i=1;i<(SizeX-1);i++)
			phisto[image[i+j*SizeX]]++;

	/*** Compute cumulated histogram ***/

	phisto_cumul[0]=phisto[0];
	for (i=1;i<256;i++) phisto_cumul[i]=phisto_cumul[i-1]+phisto[i];

	/*** Compute vec_histo ***/

	for (j=1;j<(SizeY-1);j++)
		for (i=1;i<(SizeX-1);i++)
		{
			coord=i+j*SizeX;
			vec_histo[phisto_cumul[image[coord]]]=coord;
			phisto_cumul[image[coord]]--;
		}
	for (i=0;i<256;i++) phisto_cumul[i]=phisto_cumul[i]+phisto[i];

	/*** Init aux and result ***/

	copy(mark,aux,SizeX,SizeY);
	copy(mark,result,SizeX,SizeY);

	/*** Main loop ***/

	for (seuil=255;seuil>=0;seuil--)
		if (phisto[seuil]>0)
		{
			for (i=0;i<phisto[seuil];i++)
			{
				coord=vec_histo[phisto_cumul[seuil]-i];
				if ((aux[coord]!=255) && (aux[coord+1]!=255) && (aux[coord-1]!=255)
						&& (aux[coord+SizeX]!=255) && (aux[coord-SizeX]!=255))
					aux[coord]=200;
				else
				{
					aux[coord]=255;
					result[coord]=seuil;
					k=l=0;
					ok=0;
					while (ok==0)
					{
//						if (k>(SizeX*SizeY-10));
//						if (l>(SizeX*SizeY-10));
						if (aux[coord+1]==200)
						{
							pass=0;
							for (j=l;j<=k;j++)
								if (list[j]==coord+1) pass=1;
							if (pass==0)
							{
								list[k]=coord+1;
								k++;
							}
						}
						if (aux[coord-1]==200)
						{
							pass=0;
							for (j=l;j<=k;j++)
								if (list[j]==coord-1) pass=1;
							if (pass==0)
							{
								list[k]=coord-1;
								k++;
							}
						}
						if (aux[coord-SizeX]==200)
						{
							pass=0;
							for (j=l;j<=k;j++)
								if (list[j]==coord-SizeX) pass=1;
							if (pass==0)
							{
								list[k]=coord-SizeX;
								k++;
							}
						}
						if (aux[coord+SizeX]==200)
						{
							pass=0;
							for (j=l;j<=k;j++)
								if (list[j]==coord+SizeX) pass=1;
							if (pass==0)
							{
								list[k]=coord+SizeX;
								k++;
							}
						}
						if (list[l]==0) ok=1;
						else
						{
							aux[list[l]]=255;
							result[list[l]]=seuil;
							coord=list[l];
							list[l]=0;
							l++;
						}
					}
				}
			}
		}
	/*** end ***/

	invert(result,image,SizeX,SizeY);
	free(vec_histo);
	free(list);
	free(phisto_cumul);
	free(phisto);
	free(result);
	free(aux);
}
/***********************************************************************/
void cumul_freq(unsigned char *image,int *cumu,int *Min,int *Max)
{
	unsigned char *f;
	int x,y,coord;

	f=image;
	for (x=0;x<M;x++)
		for (y=0;y<N;y++)
		{
			coord = x + y*M;
			cumu[f[coord]]++;
		}
	for (x=1;x<=255;x++)
	{
		cumu[x] = cumu[x]       + cumu[x-1];
	}
	x = 0;
	while (cumu[x]==0)
	{
		x++;
	}
	*Min = x;
	x = 255;
	while(cumu[x]==cumu[x-1])
	{
		x--;
	}
	*Max = x;
}
/***********************************************************************/
void construct_imi(unsigned char *image,int *cumu,int *niter,int *imi)
{
	unsigned char *f;
	int x,y,coord;

	f=image;
	for (x=0;x<M;x++)
		for (y=0;y<N;y++)
		{
			coord= x + y*M;
			if (f[coord]==0)
			{
				imi[niter[f[coord]]] = coord;
			}
			else
			{
				imi[cumu[f[coord]-1]+niter[f[coord]]] = coord;
			}
			niter[f[coord]]++;
		}
}
/***********************************************************************/
void fifo_add(int coord)
{
	queue[PrtLast] = coord;
	PrtLast++;
	if (PrtLast==M*N) PrtLast = 0;
}
/***********************************************************************/
int fifo_first()
{
	int ret;
	ret = queue[PrtFirst];
	queue[PrtFirst] = -1;
	PrtFirst++;
	if (PrtFirst==M*N) PrtFirst = 0;
	return ret;
}
/***********************************************************************/
int fifo_empty()
{
	int ret=1;
	if (queue[PrtFirst]!=-1) ret=0;
	return ret;
}
/*************************************************************************/
void watershed(unsigned char *image,int xSize,int ySize,int *starimo,int *NZone,char avec0)
{
	unsigned char *f;
	int fin,Min,Max,inc,NPoints,x,y,i,j,h,p,pprime,psecond,coord,coordij,coordinf,CurrentLabel,CurrentDist,cond;
	int *imi,*imd,*imo;
	int *cumu,*iterations;

	f=image;
	M = xSize;
	N = ySize;

	imo=starimo;
	for (x=0;x<M*N;x++)
	{
		imo[x] = _INIT_;
	}

	if ((imi=(int *)calloc(M*N,sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}

	if ((imd=(int *)calloc(M*N,sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}

	if ((cumu=(int *)calloc(256,sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if ((iterations=(int *)calloc(256,sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	if ((queue=(int *)calloc(M*N,sizeof(int)))==NULL) /* Avant: /4 */
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}
	for (x=0;x<M*N;x++)
	{
		queue[x] = -1;
	}
	cumul_freq(image,cumu,&Min,&Max);
	construct_imi(image,cumu,iterations,imi);
	PrtFirst = 0;
	PrtLast  = 0;
	CurrentLabel = 0;
	free(iterations);
	for (h=Min;h<=Max;h++)
	{
		if (h==0)
		{
			coordinf = 0;
		}
		else
		{
			coordinf = cumu[h-1];
		}
		NPoints = cumu[h]-coordinf;
		for (inc=0;inc<NPoints;inc++)
		{
			coord = imi[coordinf+inc];
			imo[coord] = _MASK_;
			y = coord/M;
			x = coord - y*M;
			cond = 0;
			for (i=-1;i<=1;i=i+2)
			{
				if ( ((i+x)>=0)&&((i+x)<M) )
				{
					pprime = coord + i;
					if ( (imo[pprime]>0)||(imo[pprime]==_WSHED_) )
					{
						cond=1;
					}
				}
			}
			for (j=-1;j<=1;j=j+2)
			{
				if ( ((j+y)>=0)&&((j+y)<N) )
				{
					pprime = coord + j*M;
					if ( (imo[pprime]>0)||(imo[pprime]==_WSHED_) )
					{
						cond=1;
					}
				}
			}
			if (cond==1)
			{
				imd[coord] = 1;
				fifo_add(coord);
			}
		}
		CurrentDist = 1;
		fifo_add(-2);
		while(1)
		{
			p = -1;
			while (p==-1)
			{
				p = fifo_first();
//				if (p==-1) ;
			}
			if (p==-2)
			{
				if (fifo_empty()==1) break;
				else
				{
					fifo_add(-2);
					CurrentDist++;
					p = -1;
					while (p==-1)
					{
						p = fifo_first();
//						if (p==-1) ;
					}
				}
			}
			y = p/M;
			x = p - y*M;
			for (i=-1;i<=1;i=i+2)
			{
				if ( ((i+x)>=0)&&((i+x)<M) )
				{
					pprime = p + i;
					if ( (imd[pprime]<CurrentDist)&&
							( (imo[pprime]>0)||(imo[pprime]==_WSHED_) ) )
					{
						if (imo[pprime]>0)
						{
							if ( (imo[p]==_MASK_)||(imo[p]==_WSHED_) )
							{
								imo[p] = imo[pprime];
							}
							else
							{
								if (imo[p]!=imo[pprime])
								{
									imo[p] = _WSHED_;
								}
							}
						}
						else if (imo[p]==_MASK_) imo[p]=_WSHED_;
					}
					else if ( (imo[pprime]==_MASK_)&&(imd[pprime]==0) )
					{
						imd[pprime] = CurrentDist + 1;
						fifo_add(pprime);
					}
				}
			}
			for (j=-1;j<=1;j=j+2)
			{
				if ( ((j+y)>=0)&&((j+y)<N) )
				{
					pprime = p + j*M;
					if ( (imd[pprime]<CurrentDist)&&
							( (imo[pprime]>0)||(imo[pprime]==_WSHED_) ) )
					{
						if (imo[pprime]>0)
						{
							if ( (imo[p]==_MASK_)||(imo[p]==_WSHED_) )
							{
								imo[p] = imo[pprime];
							}
							else
							{
								if (imo[p]!=imo[pprime])
								{
									imo[p] = _WSHED_;
								}
							}
						}
						else if (imo[p]==_MASK_) imo[p]=_WSHED_;
					}
					else if ( (imo[pprime]==_MASK_)&&(imd[pprime]==0) )
					{
						imd[pprime] = CurrentDist + 1;
						fifo_add(pprime);
					}
				}
			}
		}
		if (h==0)
		{
			coordinf        = 0;
		}
		else
		{
			coordinf = cumu[h-1];
		}
		for (inc=0;inc<NPoints;inc++)
		{
			p = imi[coordinf+inc];
			imd[p]=0;
			if (imo[p]==_MASK_)
			{
				CurrentLabel++;
				fifo_add(p);
				imo[p]=CurrentLabel;
				while (fifo_empty()==0)
				{
					pprime = -1;
					while (pprime==-1)
					{
						pprime = fifo_first();
//						if (pprime==-1) ;
					}
					y = pprime/M;
					x = pprime - y*M;
					for (i=-1;i<=1;i=i+2)
					{
						if ( ((i+x)>=0)&&((i+x)<M) )
						{
							psecond = pprime+i;
							if (imo[psecond]==_MASK_)
							{
								fifo_add(psecond);
								imo[psecond]=CurrentLabel;
							}
						}
					}
					for (j=-1;j<=1;j=j+2)
					{
						if ( ((j+y)>=0)&&((j+y)<N) )
						{
							psecond = pprime + j*M;
							if (imo[psecond]==_MASK_)
							{
								fifo_add(psecond);
								imo[psecond]=CurrentLabel;
							}
						}
					}
				}
			}
		}
	}

	/**** Pixels borders = 0, others: label ****/

	if (avec0==1)
	{
		for (x=0;x<M;x++)
			for (y=0;y<N;y++)
			{
				coord = x + y*M;
				for (i=-1;i<=1;i=i+2)
				{
					coordij = coord + i;
					if ( ((x+i)>=0)&&((x+i)<M) )
					{
						if ( (imo[coordij]<imo[coord])&&
								(imo[coordij]!=_WSHED_) )
						{
							imo[coord]=_WSHED_;
						}
					}
				}
				for (j=-1;j<=1;j=j+2)
				{
					coordij = coord + j*M;
					if ( ((y+j)>=0)&&((y+j)<N) )
					{
						if ( (imo[coordij]<imo[coord])&&
								(imo[coordij]!=_WSHED_) )
						{
							imo[coord]=_WSHED_;
						}
					}
				}
			}
	}

	/**** No border pixels ****/

	if (avec0==0)
	{
		fin=1;
		while (fin==1)
		{
			fin=0;
			for (x=0;x<M*N;x++)
			{
				imi[x] = 0;
			}
			for (x=0;x<M;x++)
				for (y=0;y<N;y++)
				{
					coord = x + y*M;
					if (imo[coord]==_WSHED_)
					{
						fin = 1;
						Max = 0;
						for (i=-1;i<=1;i++)
							for (j=-1;j<=1;j++)
							{
								coordij = coord + i +j*M;
								if ( ((x+i)>=0)&&((x+i)<M)&&((y+j)>=0)&&((y+j)<N) )
								{
									if (imo[coordij]>Max) Max=imo[coordij];
								}
							}
						imi[coord] = Max;
					}
				}
			for (x=0;x<M*N;x++)
			{
				if (imi[x]!=0) imo[x]=imi[x];
			}
		}
	}
	*NZone = CurrentLabel;
	free(cumu);
	free(queue);
	free(imi);
	free(imd);
}
/*************************************************************************/

void segmentation(unsigned char *fi2,char merge,char color,int M,int N,unsigned int *label,int *zones)
{
	int n_zone,i,j;
	unsigned char *ima;
	int *imawa;

	if ((imawa=(int *)calloc(M*N,sizeof(int)))==NULL)
	{
		printf("Memory problem (watershed)\n");
		exit(0);
	}

	ima = fi2;
	if (merge==0)
	{
		watershed(ima,M,N,imawa,&n_zone,1);
		for (j=0;j<N;j++)
			for (i=0;i<M;i++)
				label[i+j*M]=imawa[i+j*M];
		for (j=0;j<N;j++)
			for (i=0;i<M;i++)
				if ((imawa[i+j*M])==0) fi2[i+j*M]=0;
				else fi2[i+j*M]=255;
	}
	else
	{
		watershed(ima,M,N,imawa,&n_zone,0);
		for (j=0;j<N;j++)
			for (i=0;i<M;i++)
				label[i+j*M]=imawa[i+j*M];
		for (j=1;j<N;j++)
			for (i=0;i<M;i++)
			{
				if (label[i+j*M]!=label[i+j*M-M]) fi2[i+j*M]=0;
				else fi2[i+j*M]=255;
			}
		for (j=0;j<N;j++)
			for (i=1;i<M;i++)
			{
				if (label[i+j*M]!=label[i+j*M-1]) fi2[i+j*M]=0;
			}
	}
	(*zones)=n_zone;
	free(imawa);

}
