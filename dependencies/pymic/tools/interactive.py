__author__ = 'olivier'
"""Interactive window that allows to test various parameters of a process"""

from enthought.traits.api import HasTraits, Instance, Enum, Range, Dict, Button, Int, Bool, Str, Property,Event, Array, DelegatesTo, on_trait_change,List, Tuple, String
from enthought.traits.ui.api import Item, Group, View, InstanceEditor, RangeEditor, HGroup, HFlow, VGroup
from enthought.chaco.api import ArrayPlotData, Plot, AbstractOverlay, gray,jet,hot,DataRange1D, ScatterInspectorOverlay
from enthought.chaco.tools.api import PanTool, MoveTool, ZoomTool, ScatterInspector, DragTool, BroadcasterTool
from enthought.enable.component_editor import ComponentEditor
from enthought.enable.api import ComponentEditor, BaseTool
from enthought.traits.api import Button, CArray, Bool, Float, Range, \
                                 HasTraits, Instance, Property, Directory, File
from enthought.traits.ui.api import Item, View, Group, RangeEditor, Tabbed, VSplit, HSplit,\
                                    HGroup, Handler, spring, DirectoryEditor , FileEditor, BooleanEditor, TextEditor, Menu, MenuBar, Action
from enthought.traits.ui.api \
    import View, Item, Tabbed, TableEditor, ListEditor

import numpy as np

class Process(HasTraits):
    #define here the parameters of the process
    value = Range(0.0,1.0,0.1)
    traits_view = View(VGroup(
                        Item('threshold')))

    def __init__(self):
        #add here some initialisation if needed
        pass

    def apply(self,input):
        #here is the code that return an image from the input image
        print 'exec '
        return input>self.value

class MyProcess(Process):
    threshold1 = Range(0.0,1.0,0.1)
    threshold2 = Range(0.0,1.0,0.2)
    traits_view = View(VGroup(
                    Item('threshold1'),
                    Item('threshold2')))
    def __init__(self,input):
        #add here some initialisation if needed
        self.data = input

    def apply(self):
        #here is the code that return an image from the input image
        return (self.data<self.threshold2)&(self.data>self.threshold1)


class SimpleClickTool(BaseTool):
    right_click_position = Array()
    middle_click_position = Array()

#    def normal_mouse_move(self, event):
#        print "Screen point:", event.x, event.y

    def __init__(self,plot):
        super(SimpleClickTool,self).__init__(plot)

    def normal_middle_down(self, event):
        self.middle_click_position =  self.component.map_data((event.x, event.y))

    def normal_right_down(self, event):
#        print "Mouse went down at", event.x, event.y
#        print "Data:", self.component.map_data((event.x, event.y))
        self.right_click_position =  self.component.map_data((event.x, event.y))

#    def normal_left_up(self, event):
#        print "Mouse went up at:", event.x, event.y

class Tester(HasTraits):
    plotin = Plot
    plotout = Plot
    process = Instance(Process)
    input = Array()
    output = Array()

    auto = Bool(True)
    button = Button('Apply')

    def traits_view(self):

        # Create the list of Actions for the 'Item' menu.
#        item_actions = [ActionWithArgs(name=item_name, on_perform=self.new_item, args=(item_name,))
#                                                            for item_name in self.item_names]

        # Create the menus and the menubar.
#        item_menu = Menu(*item_actions, name="Item")
        menubar = \
            MenuBar(
                Menu(
                    Menu(
#                        item_menu,
                        name="Export",
                    ),
                    name="File",
                )
            )

        # Create the View.
        view = View(VGroup(
                        Item('process',editor = InstanceEditor(),style = 'custom',show_label = False,resizable=False),
                        HGroup(Item('auto'),Item('button',enabled_when = 'not object.auto')),
                        Tabbed(
                        Item("plotout",editor = ComponentEditor(),style = 'custom',show_label = False,resizable=True,label='result'),
                        Item("plotin",editor = ComponentEditor(),style = 'custom',show_label = False,resizable=True,label='original')
                        )),title=self.process.__class__.__name__,resizable=True,menubar=menubar)

        return view


    def __init__(self,process):
        self.plotin = Plot()
        self.plotout = Plot()
        self.plotdatain = ArrayPlotData()
        self.plotdataout = ArrayPlotData()
        self.plotin.data = self.plotdatain
        self.plotout.data = self.plotdataout
        self.plotdatain.set_data('imagedata',np.ndarray((10,10)))
        self.plotdataout.set_data('imagedata',np.ndarray((10,10)))
        self.image = self.plotin.img_plot("imagedata")
        self.image = self.plotout.img_plot("imagedata")

        # Attach some tools to the plot (tools are synchronized with a broadcaster)
        broadcaster = BroadcasterTool()
        broadcaster.tools.append(PanTool(self.plotin))
        broadcaster.tools.append(PanTool(self.plotout))

        for c in (self.plotin, self.plotout):
            zoom = ZoomTool(component=c, tool_mode="box", always_on=False)
            broadcaster.tools.append(zoom)

        self.plotin.tools.append(broadcaster)
        self.plotout.tools.append(broadcaster)

        self.click_tool = SimpleClickTool(self.plotout)
        self.plotout.tools.append(self.click_tool)

        print 'init done'
        self.process = process

        self.process.on_trait_change(self._process_changed)
        self.click_tool.on_trait_change(self._right_click_position_changed)

    def _process_changed(self):
        if self.auto:
            self._button_changed()

    def _button_changed(self):
        self.output = self.process.apply()
        self.plotdataout.set_data('imagedata',self.output)
        self.plotdatain.set_data('imagedata',self.process.data)

    def _right_click_position_changed(self):
        if len(self.click_tool.right_click_position):
            x,y = self.click_tool.right_click_position
            value = self.output[int(y),int(x)]
            print 'sample[%d,%d] : %f'%(x,y,value)



if __name__ == "__main__":

    ima = np.random.random((100,100))

    process = MyProcess(ima)
    tester = Tester(process)

    tester.configure_traits()